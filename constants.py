import os 

APP_VERISON = os.getenv('APP_VERISON') if os.getenv('APP_VERISON') else "1.0.0"

HOST = os.getenv('HOST') if os.getenv('HOST') else "127.0.0.1"
PORT = os.getenv('PORT') if os.getenv('PORT') else 6094
PARAM_A = os.getenv('PARAM_A') if os.getenv('PARAM_A') else 'abbonamentoAttuale'
PARAM_B = os.getenv('PARAM_B') if os.getenv('PARAM_B') else 'abbonamentoNuovo'
DEBUG = os.getenv('DEBUG') if os.getenv('DEBUG') else True
RESPONSE_FIELD_OK = os.getenv('RESPONSE_FIELD_OK') if os.getenv('RESPONSE_FIELD_OK') else "equal"
RESPONSE_FIELD_ERROR = os.getenv('RESPONSE_FIELD_ERROR') if os.getenv('RESPONSE_FIELD_ERROR') else "error"

MISSING_PARAMETER = 'The mandatory parameter \'{0}\' is missing'