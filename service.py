#!/usr/bin/env python3
from http.client import INTERNAL_SERVER_ERROR
from flask import Flask
from flask import request
from flask_cors import CORS, cross_origin
import json
import constants


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/v1/compare", methods=['POST'])
@cross_origin()
def compare():
    try:
        r = request.get_json(silent=True)

        if constants.PARAM_A not in r:
            return {constants.RESPONSE_FIELD_ERROR: constants.MISSING_PARAMETER.format(constants.PARAM_A) }, 400

        if constants.PARAM_B not in r:
            return {constants.RESPONSE_FIELD_ERROR: constants.MISSING_PARAMETER.format(constants.PARAM_B) }, 400

        a, b = json.dumps(r[constants.PARAM_A], sort_keys=True), json.dumps(r[constants.PARAM_B], sort_keys=True)

        response = {constants.RESPONSE_FIELD_OK: a == b}, 200

        return response


    except Exception as error:
        return {
                   "error_class": error.__class__.__name__,
                   "message": f"{error}"
               }, 500

if __name__ == '__main__':
    print("App version:", constants.APP_VERISON)
    app.run(host=constants.HOST, port=constants.PORT, debug=constants.DEBUG)
