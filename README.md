# Installation #

Make sure you have python 3.x installed on your machine. You can check this with the command

`python --verison` --> `>> Python 3.8.6`

In order to execute the microservice run the following commands: 

```
python -m venv ./venv # create the virtual environment 

source venv/bin/activate # linux
.\venv\Scripts\activate # windows

pip install -r requirements.txt # install dependencies 

python service.py # run the service
```



If you change the dependencies list, export the list before commit with the following command: 

```
pip freeze > requirements.txt
```

## Manual dependencies installation

```
pip install flask
pip install flask flask-cors
```



# Test

- POST http://127.0.0.1:8080/compare

Json Body: 

```
{
	"a": {"key": "value"},
	"b": {"key": "value"}
}
```

Response: 

```
{
	"result": true
}
```

----------------

POST http://127.0.0.1:8080/compare

Json Body: 

```
{
	"a": {"key": "value"},
	"b": {"key": "value_2"}
}
```

Response: 

```
{
	"result": false
}
```

