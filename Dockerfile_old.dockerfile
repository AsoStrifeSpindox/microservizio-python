# start by pulling the python image
FROM python:3.8-alpine

ENV APP_NAME=siae.python.sport

# copy every content from the local file to the image
COPY . /usr/${APP_NAME}

# switch working directory
WORKDIR /usr/${APP_NAME}

# install the dependencies and packages in the requirements file
RUN pip install -r requirements.txt

EXPOSE 6094

# configure the container to run in an executed manner
ENTRYPOINT [ "python" ]

CMD ["service.py" ]